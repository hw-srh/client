import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import PageRouter from "./components/page-router";
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import './index.css';

ReactDOM.render((
    <div className="index-content">
        <React.StrictMode>
            <BrowserRouter
                keyLength={50}
                basename={"/app"}
                forceRefresh={false}
            >
                <PageRouter/>
            </BrowserRouter>
        </React.StrictMode>
    </div>
    ),
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
