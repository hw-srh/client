import {CreateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {FilesRowsToJsonConverter} from "./files-rows-to-json-converter";

export class TimeTrackingExcelToJsonConverter extends FilesRowsToJsonConverter {

    private readonly EXCEL_COLUMN_NAMES = [
        "email",
        "customerName",
        "customerDescription",
        "taskName",
        "taskDescription",
        "scheduledStart",
        "scheduledEnd",
        "scheduledBreakTime",
        "actualStart",
        "actualEnd",
        "actualBreakTime",
        "workProtocol",
    ];

    async convert(): Promise<CreateTimeTrackingDto[]> {
        const bullshit = await super.convert();
        const modifiedData = [];
        for(const dataset of bullshit) {
            dataset.actualStart = new Date(dataset.actualStart);
            dataset.actualEnd = new Date(dataset.actualEnd);
            dataset.scheduledStart = new Date(dataset.scheduledStart);
            dataset.scheduledEnd = new Date(dataset.scheduledEnd);
            modifiedData.push(dataset);
        }
        return modifiedData as CreateTimeTrackingDto[];
    }

    protected getExcelColumnNames(): string[] {
        return this.EXCEL_COLUMN_NAMES;
    }
}