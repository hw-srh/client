import {FilesRowsToJsonConverter} from "./files-rows-to-json-converter";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";

export class UsersExcelToJsonConverter extends FilesRowsToJsonConverter {

    private readonly EXCEL_COLUMN_NAMES: Array<keyof CreateUserDto> = [
        "email",
        "firstname",
        "lastname",
        "workHoursPerWeek"
    ];

    protected getExcelColumnNames(): string[] {
        return this.EXCEL_COLUMN_NAMES;
    }
}