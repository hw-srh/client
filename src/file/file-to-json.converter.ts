import XLSX, {WorkBook, WorkSheet} from "xlsx";

export class ExcelFilesToJsonConverter {

    constructor(private customExcelColumnNames: string[], private files: ArrayBuffer[]) {
    }

    convert(): { [key: string]: any }[] {

        const configs: { [key: string]: any }[] = [];

        for (const file of this.files) {
            configs.push(...this.convertExcelFileRowsToJsonObjects(file));
        }

        return configs;
    }

    private convertExcelFileRowsToJsonObjects(file: ArrayBuffer): { [key: string]: any }[] {

        const rows: { [key: string]: any }[] = [];

        const workBook: WorkBook = XLSX.read(file, {type: "array"});

        for (const sheetName of workBook.SheetNames) {
            const sheet: WorkSheet = workBook.Sheets[sheetName];
            const result: { [key: string]: any }[] = XLSX.utils.sheet_to_json(
                sheet,
                {
                    header: this.customExcelColumnNames
                }
            );

            result.shift(); // remove first row because that row usually only contains the column names

            rows.push(...result)
        }

        return rows;
    }
}