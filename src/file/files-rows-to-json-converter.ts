import {FilesToMemoryReader} from "./files-to-memory-reader";
import {ExcelFilesToJsonConverter} from "./file-to-json.converter";

export abstract class FilesRowsToJsonConverter {

    constructor(private filesToMemoryReader: FilesToMemoryReader) {
    }

    async convert(): Promise<{ [key: string]: any }[]> {
        const filesInMemory = await this.filesToMemoryReader.openFileDialogAndReadFilesIntoMemory();
        const excelFilesToJsonConverter = new ExcelFilesToJsonConverter(this.getExcelColumnNames(), filesInMemory);
        return excelFilesToJsonConverter.convert();
    }

    protected abstract getExcelColumnNames(): string[];
}