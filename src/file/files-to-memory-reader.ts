export class FilesToMemoryReader {

    private filesInMemory: ArrayBuffer[] = [];

    async openFileDialogAndReadFilesIntoMemory(): Promise<ArrayBuffer[]> {

        this.filesInMemory = [];

        return new Promise<ArrayBuffer[]>((resolve, reject) => {
            const input = document.createElement('input');
            input.type = 'file';
            input.onchange = (e: any) => this.onFilesSelected(e, resolve);
            input.onabort = (e: any) => reject();
            input.click();
        });
    }

    private readeFileIntoMemory(readerEvent: ProgressEvent<FileReader>) {
        const rawExcelFileContent: ArrayBuffer = readerEvent?.target?.result as ArrayBuffer ?? new ArrayBuffer(0);

        if (rawExcelFileContent.byteLength > 0) {
            const excelFileData = new Uint8Array(rawExcelFileContent);
            this.filesInMemory.push(excelFileData);
        }
    }

    private onFilesSelected(e: any, resolve: any) {
        const files: FileList = e?.target?.files ?? [];
        for (let i = 0; i < files.length; i++) {
            const reader = new FileReader();
            reader.readAsArrayBuffer(files[i]);
            reader.onload = (readerEvent: ProgressEvent<FileReader>) => {
                this.readeFileIntoMemory(readerEvent);
                if ((i + 1) === files.length) {
                    resolve(this.filesInMemory);
                }
            }
        }
    }
}