import {MessageBroker} from "../message-broker";
import {AxiosResponse} from "axios";
import {CreateTimeTrackingDto} from "../../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {ResponseTimeTrackingDto} from "../../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {UpdateTimeTrackingDto} from "../../common/shared_interfaces/dto/time-tracking/update-time-tracking.dto";
import {RawResponseTimeTrackingDto} from "../../common/shared_interfaces/dto/time-tracking/raw-response-time-tracking.dto";
import {FilterTimeTrackingDtoWithTimeRange} from "../../common/shared_interfaces/dto/time-tracking/FilterTimeTrackingDtoWithTimeRange";
import {FilterTimeTrackingWithoutTimeRangeDto} from "../../common/shared_interfaces/dto/time-tracking/filter-time-tracking-without-time-range.dto";

export class TimeTrackingClient {

    private static rawResponseTransformConfig = {
        transformResponse: [function (data: string) {
            const parsedBody: RawResponseTimeTrackingDto[] = JSON.parse(data);

            if (!Array.isArray(parsedBody)) {
                return [];
            }

            return parsedBody.map((rawTimeTrackingDto: RawResponseTimeTrackingDto): ResponseTimeTrackingDto => {
                return {
                    ...rawTimeTrackingDto,
                    actualEnd: rawTimeTrackingDto.actualEnd ? new Date(rawTimeTrackingDto.actualEnd) : undefined,
                    actualStart: new Date(rawTimeTrackingDto.actualStart),
                    scheduledStart: new Date(rawTimeTrackingDto.scheduledStart),
                    scheduledEnd: new Date(rawTimeTrackingDto.scheduledEnd),
                };
            });
        }],
    }

    constructor(private messageBroker: MessageBroker) {
    }

    async create(createTimeTrackingDTOs: CreateTimeTrackingDto[] | CreateTimeTrackingDto): Promise<AxiosResponse<ResponseTimeTrackingDto[]>> {
        if (!Array.isArray(createTimeTrackingDTOs)) {
            createTimeTrackingDTOs = [createTimeTrackingDTOs];
        }
        return this.messageBroker.sendPostRequest(
            MessageBroker.constructUrl('/time-tracking'), createTimeTrackingDTOs,
            TimeTrackingClient.rawResponseTransformConfig
        );
    }

    async update(updateTimeTrackingDTOs: UpdateTimeTrackingDto[] | UpdateTimeTrackingDto): Promise<AxiosResponse<ResponseTimeTrackingDto[]>> {
        if (!Array.isArray(updateTimeTrackingDTOs)) {
            updateTimeTrackingDTOs = [updateTimeTrackingDTOs];
        }
        return this.messageBroker.sendPatchRequest(
            MessageBroker.constructUrl('/time-tracking'), updateTimeTrackingDTOs,
            TimeTrackingClient.rawResponseTransformConfig
        );
    }

    async delete(id: number): Promise<AxiosResponse<any>> {
        return this.messageBroker.sendDeleteRequest(MessageBroker.constructUrl(`/time-tracking/${id}`));
    }

    async findAll(filterTimeTrackingDto: FilterTimeTrackingDtoWithTimeRange | FilterTimeTrackingWithoutTimeRangeDto): Promise<AxiosResponse<ResponseTimeTrackingDto[]>> {

        const queryString = [];

        for (let [key, value] of Object.entries(filterTimeTrackingDto)) {
            const uriKey = key.replace(/[A-Z]/g, (match) => `-${match.toLowerCase()}`);
            // @ts-ignore
            queryString.push(`${uriKey}=${encodeURIComponent(value)}`);
        }

        return this.messageBroker.sendGetRequest(
            MessageBroker.constructUrl(`/time-tracking?${queryString.join('&')}`),
            TimeTrackingClient.rawResponseTransformConfig
        );
    }
}