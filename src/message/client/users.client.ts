import {MessageBroker} from "../message-broker";
import {CreateUserDto} from "../../common/shared_interfaces/dto/user/create-user.dto";
import {AxiosResponse} from "axios";
import {ResponseUserDto} from "../../common/shared_interfaces/dto/user/response-user.dto";
import {UpdateUserIncludingPasswordDto} from "../../common/shared_interfaces/dto/user/update-user-including-password.dto";
import {UpdateUserExceptPasswordDto} from "../../common/shared_interfaces/dto/user/update-user-except-password.dto";

export class UsersClient {

    constructor(private messageBroker: MessageBroker) {
    }

    async create(createUserDto: CreateUserDto | CreateUserDto[]): Promise<AxiosResponse<ResponseUserDto[]>> {
        if (!Array.isArray(createUserDto)) {
            createUserDto = [createUserDto];
        }
        return this.messageBroker.sendPostRequest(MessageBroker.constructUrl('/users'), createUserDto);
    }

    async update(updateUserDto: UpdateUserIncludingPasswordDto | UpdateUserExceptPasswordDto): Promise<AxiosResponse<ResponseUserDto>> {
        return this.messageBroker.sendPatchRequest(MessageBroker.constructUrl('/users'), updateUserDto);
    }

    async delete(id: number): Promise<AxiosResponse<any>> {
        return this.messageBroker.sendDeleteRequest(MessageBroker.constructUrl(`/users/${id}`));
    }

    async findOne(id: number): Promise<AxiosResponse<ResponseUserDto>> {
        return this.messageBroker.sendGetRequest(MessageBroker.constructUrl(`/users/${id}`));
    }

    async findAll(): Promise<AxiosResponse<ResponseUserDto[]>> {
        return this.messageBroker.sendGetRequest(MessageBroker.constructUrl('/users'));
    }

    async findOtherUsers(): Promise<AxiosResponse<ResponseUserDto[]>> {
        return this.messageBroker.sendGetRequest(MessageBroker.constructUrl('/users/others'));
    }
}