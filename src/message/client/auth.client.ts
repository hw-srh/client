import axios, {AxiosError, AxiosResponse} from "axios";
import {loginPagePath} from "../../page-registry";
import {LoginDto} from "../../common/shared_interfaces/dto/login/login.dto";
import {PageProps} from "../../interfaces/page-props";
import {MessageBroker} from "../message-broker";

export class AuthClient {

    constructor(private pageProps: PageProps) {
    }

    async logout() {
        try {
            await this.pageProps.messageBroker.sendDeleteRequest(MessageBroker.constructUrl('/auth/logout'));
        } catch (e) {
            console.log('Logout request failed, removing token on client side anyway');
        }
        this.pageProps.jwtManager.invalidateToken();
        this.pageProps.changePage(loginPagePath);
    }

    async login(loginDto: LoginDto): Promise<void> {

        if (this.pageProps.jwtManager.hasToken()) {
            if (!this.pageProps.jwtManager.hasTokenExpired()) {
                return;
            }

            try {
                await this.pageProps.messageBroker.refreshToken();
                return;
            } catch (error) {
                error = error as AxiosError;
                if (error.response.status !== 401) {
                    throw error;
                }
            }
        }

        // using axios here as opposed to the message broker because we may not want to redirect on auth error on the login page
        return axios.post(MessageBroker.constructUrl('/auth/login'), loginDto)
            .then(
                (response: AxiosResponse<{ access_token: string }>) => {
                    return this.pageProps.jwtManager.setNewToken(response.data?.access_token ?? '');
                })
            .catch((error) => {
                this.pageProps.jwtManager.invalidateToken();
            });
    }
}