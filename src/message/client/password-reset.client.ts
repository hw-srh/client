import axios, {AxiosResponse} from "axios";
import {PasswordResetDto} from "../../common/shared_interfaces/dto/password-reset/password-reset.dto";
import {MessageBroker} from "../message-broker";

export class PasswordResetClient {

    async requestPasswordResetLink(email: string): Promise<AxiosResponse> {
        // using axios here as opposed to the message broker because we dont have a jwt token yet
        return axios.post(MessageBroker.constructUrl(`/password-reset/${encodeURIComponent(email)}`));
    }

    async resetPassword(passwordResetDto: PasswordResetDto): Promise<void> {
        // using axios here as opposed to the message broker because we dont have a jwt token yet
        return axios.post(MessageBroker.constructUrl('/password-reset'), passwordResetDto);
    }
}