import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {JwtManager} from "../auth/jwt-manager";
import {loginPagePath} from "../page-registry";
import {JwtNotFoundException} from "../exceptions/jwt-not-found-exception";


export class MessageBroker {

    constructor(
        private readonly jwtManager: JwtManager,
        private readonly changePage: (path: string) => void
    ) {
    }

    public static constructUrl(uri: string): string {
        return `${process.env.REACT_APP_DEV_SERVER_URL ?? window.location.origin}${uri}`;
    }

    public async sendPostRequest<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
        const request = async () => axios.post<T, R>(url, data, await this.injectJwtAuthenticationHeader(config));
        return await this.sendRequest(request);
    }

    public async sendGetRequest<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        const request = async () => axios.get<T, R>(url, await this.injectJwtAuthenticationHeader(config));
        return await this.sendRequest(request);
    }

    public async sendPatchRequest<T = any, R = AxiosResponse<T>>(url: string, data: any, config?: AxiosRequestConfig): Promise<R> {
        const request = async () => axios.patch<T, R>(url, data, await this.injectJwtAuthenticationHeader(config));
        return await this.sendRequest(request);

    }

    public async sendDeleteRequest<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        const request = async () => axios.delete<T, R>(url, await this.injectJwtAuthenticationHeader(config));
        return await this.sendRequest(request);

    }

    private async injectJwtAuthenticationHeader(config?: AxiosRequestConfig): Promise<undefined | AxiosRequestConfig> {
        return {
            ...config,
            headers: {
                ...config?.headers,
                Authorization: `Bearer ${await this.getToken()}`
            }
        };
    }

    private async getToken(): Promise<string> {
        const token = await this.jwtManager.getToken();

        if (!token) {
            this.changePage(loginPagePath);
            throw new JwtNotFoundException('Missing JWT, re authentication required!');
        }

        return token;
    }

    async refreshToken() {
        const response = await this.sendPostRequest(MessageBroker.constructUrl('/auth/refresh'));
        await this.jwtManager.setNewToken(response.data.access_token);
    }

    private async sendRequest<T = any, R = AxiosResponse<T>>(request: () => Promise<R>): Promise<R> {
        try {
            return await request();
        } catch (error) {
            return this.handleAuthenticationError(request, error);
        }
    }

    private async handleAuthenticationError<T = any, R = AxiosResponse<T>>(request: () => Promise<R>, error: AxiosError): Promise<R> {

        if (error.response?.data?.message === 'jwt expired') {
            try {
                await this.refreshToken();
                return await request();
            } catch (refreshError) {
                error = refreshError;
            }
        }

        if (error.response?.status === 401) {
            this.jwtManager.invalidateToken();
            this.changePage(loginPagePath);
        }

        throw error;
    }
}