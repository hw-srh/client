export class JwtNotFoundException extends Error {
    public statusCode: number

    constructor(
        public message: string
    ) {
        super(message);
        this.statusCode = 401;
    }
}