import * as React from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import './styles/home.css';
import {AuthClient} from "../message/client/auth.client";

export function Home(props: PageProps) {

    const authClient = new AuthClient(props);
    const logout = () => authClient.logout();

    return (
        <div>
            <Menu {...props}/>
            <div>
                <h1>Home page</h1>
            </div>
            <div>
                <button onClick={logout}>Logout</button>
            </div>
        </div>
    );

}