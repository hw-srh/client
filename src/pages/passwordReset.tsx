import React, {useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import './styles/passwordReset.css';
import {PasswordResetClient} from "../message/client/password-reset.client";

interface PasswordData {
    password: string,
}

export function PasswordReset(props: PageProps) {
    const [userDataEmail, setUserDataEmail] = useState<string>("");

    const [userData, setUserData] = useState<PasswordData>({
        password: 'xyz'
    });


    const [userData2, setUserData2] = useState<PasswordData>({
        password: 'xyz'
    });


    const sendPasswordResetRequest = async (email:string, password:string) => {
        const passwordResetToken = window.location.pathname.split('/')[3] ?? '';
        const passwordResetClient = new PasswordResetClient();

        try {
            const response = await passwordResetClient.resetPassword({
                email,
                password,
                token: passwordResetToken,
            });

            alert('Password reset successful');
        } catch (e) {
            alert('Failed to reset password (token expired, invalid email or empty password');
        }
    }

    return (
        <div>
            <div>
                <div className="generic-container container">
                    <div>
                        <h1>Passwort reset</h1>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h4>Email</h4>
                            <input type="text" className="form-control" value={userDataEmail} onChange={(e:React.ChangeEvent<HTMLInputElement>)=>{
                                setUserDataEmail(e.currentTarget.value);
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Passwort</h4>
                            <input type="password" className="form-control" value={userData.password} onChange={(e:React.ChangeEvent<HTMLInputElement>)=>{
                                setUserData({
                                    password: e.currentTarget.value
                                });
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Passwort wiederholen</h4>
                            <input type="password" className="form-control" value={userData2.password} onChange={(e:React.ChangeEvent<HTMLInputElement>)=>{
                                setUserData2({
                                    password: e.currentTarget.value,
                                });
                            }}/>
                        </div>
                        <div className="col-12">
                            <button type="button" className="btn btn-primary" onClick={(e:React.MouseEvent<HTMLButtonElement>) => {
                                if(userData2.password !== userData.password) {
                                    alert("Passwörter stimmen nicht überein!");
                                } else {
                                    sendPasswordResetRequest(userDataEmail, userData.password);
                                    //ajax
                                }
                            }}>Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}