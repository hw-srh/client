import React, {useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import { PasswordResetClient } from "../message/client/password-reset.client";
import './styles/passwordLost.css';

interface PasswordLostData {
    email: string,
}

export function PasswordLost(props: PageProps) {
    const [userData, setUserData] = useState<PasswordLostData>({
        email:'test@test.de'
    });

    const clickBtn = async (e: React.MouseEvent<HTMLButtonElement>) => {
        const passwordResetClient:PasswordResetClient = new PasswordResetClient();
        await passwordResetClient.requestPasswordResetLink((document.getElementById('email') as HTMLInputElement).value);
    };

    return (
        <div>
            <Menu {...props}/>
            <div>
                <div className="generic-container container">
                    <div>
                        <h1>Passwort lost</h1>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h4>email</h4>
                            <input type="email" id="email" className="form-control" onChange={(e:React.ChangeEvent<HTMLInputElement>)=>{
                                setUserData({
                                    email: e.currentTarget.value
                                });
                            }} value={userData.email}/>
                        </div>
                        <div className="col-12">
                            <button type="button" className="btn btn-primary" onClick={clickBtn}>Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}