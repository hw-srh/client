import React, {useState} from "react";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";
import {Menu} from "../components/menu";
import {FilesToMemoryReader} from "../file/files-to-memory-reader";
import {TimeTrackingExcelToJsonConverter} from "../file/time-tracking-excel-to-json-converter";
import {UsersExcelToJsonConverter} from "../file/users-excel-to-json-converter";
import {PageProps} from "../interfaces/page-props";
import {TimeTrackingClient} from "../message/client/time-tracking.client";
import {UsersClient} from "../message/client/users.client";
import './styles/passwordLost.css';

interface UserUploadData {
}

export function UserUpload(props: PageProps) {
    const [userData, setUserData] = useState<UserUploadData>({});

    return (
        <div>
            <Menu {...props}/>
            <div>
                <div id="profil-container" className="container">
                    <div>
                        <h1>File upload</h1>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h4>Task excel file upload</h4>
                            <button type="button" className="btn btn-primary" onClick={async () => {
                                const wtf = new FilesToMemoryReader();
                                const test = new TimeTrackingExcelToJsonConverter(wtf);
                                const data = await test.convert();
                                const ttc = new TimeTrackingClient(props.messageBroker);
                                ttc.create(data).catch(() => {
                                });
                                console.log(data);
                            }}>Upload
                            </button>
                        </div>
                        <div className="col-12">
                            <h4>User excel file upload</h4>
                            <button type="button" className="btn btn-primary" onClick={async () => {
                                const wtf = new FilesToMemoryReader();
                                const test = new UsersExcelToJsonConverter(wtf);
                                const data = await test.convert();
                                const ttc = new UsersClient(props.messageBroker);
                                ttc.create(data as CreateUserDto[]).catch(() => {
                                });
                                console.log(data);
                            }}>Upload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}