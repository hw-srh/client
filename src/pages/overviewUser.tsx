import React, {useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import Select from 'react-select';
import './styles/overviewUser.css';
import {ResponseUserDto} from "../common/shared_interfaces/dto/user/response-user.dto";
import {UsersClient} from "../message/client/users.client";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";

interface UserOption {
    label: string,
    value: boolean
}

export function OverviewUser(props: PageProps) {
    const [datasets, setDatasets] = useState<ResponseUserDto[]>([]);

    const [userOptions, setUserOptions] = useState<UserOption[]>([{
        label: "Yes",
        value: true
    }, {
        label: "No",
        value: false
    }]);

    const updateDataset = async (dataset: any) => {
        const client = new UsersClient(props.messageBroker);
        client.update(dataset);
    };

    const renderDatasets = () => {
        const output: JSX.Element[] = [];
        let i: number = 0;
        for (const dataset of datasets) {
            const index: number = i;
            output.push(<tr key={i}>
                <td><input className="form-control" type="email" value={dataset.email} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const newDatasets: ResponseUserDto[] = [...datasets];
                    newDatasets[index].email = e.currentTarget.value;
                    updateDataset(newDatasets[index]);
                    setDatasets(newDatasets);
                }} /></td>
                <td><input className="form-control" type="text" value={dataset.username} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const newDatasets: ResponseUserDto[] = [...datasets];
                    newDatasets[index].username = e.currentTarget.value;
                    updateDataset(newDatasets[index]);
                    setDatasets(newDatasets);
                }} /></td>
                <td><input className="form-control" type="text" value={dataset.firstname} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const newDatasets: ResponseUserDto[] = [...datasets];
                    newDatasets[index].firstname = e.currentTarget.value;
                    updateDataset(newDatasets[index]);
                    setDatasets(newDatasets);
                }} /></td>
                <td><input className="form-control" type="text" value={dataset.lastname} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const newDatasets: ResponseUserDto[] = [...datasets];
                    newDatasets[index].lastname = e.currentTarget.value;
                    updateDataset(newDatasets[index]);
                    setDatasets(newDatasets);
                }} /></td>
                <td><input className="form-control" type="number" value={dataset.workHoursPerWeek} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const newDatasets: ResponseUserDto[] = [...datasets];
                    let val: number = parseInt(e.currentTarget.value);
                    if (isNaN(val)) {
                        val = 0;
                    }
                    newDatasets[index].workHoursPerWeek = val;
                    updateDataset(newDatasets[index]);
                    setDatasets(newDatasets);
                }} /></td>
                <td >
                    <Select
                        menuPortalTarget={document.body}
                        className="select-overview"
                        options={userOptions}
                        value={dataset.roles.includes("admin") ? userOptions[0] : userOptions[1]}
                        onChange={(e: any) => {
                            const newDatasets: ResponseUserDto[] = [...datasets];
                            if (e.value === true && !newDatasets[index].roles.includes("admin")) {
                                newDatasets[index].roles.push("admin");
                            } else if (e.value === false) {
                                const rofl: number = newDatasets[index].roles.indexOf("admin");
                                if (rofl !== -1) {
                                    newDatasets[index].roles.splice(rofl, 1);
                                }
                            }
                            updateDataset(newDatasets[index]);
                            setDatasets(newDatasets);
                        }}
                    />
                </td>
                <td>
                    <a href="#" onClick={() => { }}>delete</a>
                </td>
            </tr>);
            i++;
        }
        return output;
    }

    const fetchAll = async () => {
        const client = new UsersClient(props.messageBroker);
        client.findOtherUsers().then((data)=> {
            setDatasets(data.data);
        });
    };


    React.useEffect(() => {
        fetchAll();
    }, [])

    return (
        <div>
            <Menu {...props}/>
            <div className="container">
                <div>
                    <h1>Übersicht Benutzer</h1>
                </div>
                <div className="generic-container">
                    <div>
                        <table className="table table-striped table-dark table-responsive">
                            <thead>
                                <tr>
                                    <th scope="col-4">eMail</th>
                                    <th scope="col-3">Username</th>
                                    <th scope="col-1">Vorname</th>
                                    <th scope="col-1">Nachname</th>
                                    <th scope="col-1">Arbeitsstunden / Woche</th>
                                    <th scope="col-1">admin</th>
                                    <th scope="col-1">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {renderDatasets()}
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <button type="button" className="btn btn-primary" onClick={() => {
                            /*const newDatasets: ResponseUserDto[] = [...datasets];
                            newDatasets.push({
                                id: -1,
                                email: "",
                                username: "",
                                firstname: "",
                                lastname: "",
                                roles: [],
                                workHoursPerWeek: 30,
                            });
                            setDatasets(newDatasets);*/

                            const newDS: CreateUserDto = {
                                email: `neu${Math.floor(Math.random() * 10000 * Math.random())}@test.de`,
                                firstname: "",
                                lastname: "",
                                roles: [],
                                workHoursPerWeek: 30,
                            };

                            const client = new UsersClient(props.messageBroker);
                            client.create(newDS).then((response) => {
                                fetchAll();
                            }).catch((err)=>{
                            });
                        }}>Neuen Benutzer anlegen</button>
                    </div>
                </div>
            </div>
        </div>
    );

}