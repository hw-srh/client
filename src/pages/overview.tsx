import React, {useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import Select from 'react-select';
import './styles/overview.css';
import {EmailOption, Mocks} from "../mock/Mock";
import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {CreateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {TimeTrackingClient} from "../message/client/time-tracking.client";
import {UsersClient} from "../message/client/users.client";
import {ResponseUserDto} from "../common/shared_interfaces/dto/user/response-user.dto";
import {AxiosError, AxiosResponse} from "axios";

interface Filter {
    start: string,
    end: string,
    amount: number,
}

export function Overview(props: PageProps) {
    const [datasets, setDatasets] = useState<ResponseTimeTrackingDto[]>([]);

    const [filter, setFilter] = useState<Filter>({
        start: (() => {
            const d: Date = new Date(Date.now());
            d.setDate(d.getDate() - 5);
            let month: string = '' + (d.getMonth() + 1),
                day: string = '' + d.getDate(),
                year: string = '' + d.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        })(),
        end: (() => {
            const d: Date = new Date(Date.now());
            d.setDate(d.getDate() + 5);
            let month: string = '' + (d.getMonth() + 1),
                day: string = '' + d.getDate(),
                year: string = '' + d.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        })(),
        amount: 50,
    });

    const [userOptions, setUserOptions] = useState<EmailOption[]>([{
        label: Mocks.testEmails[0],
        value: Mocks.testEmails[0]
    }, {
        label: Mocks.testEmails[1],
        value: Mocks.testEmails[1]
    }]);

    const fetchUserOption = ():Promise<AxiosResponse<ResponseUserDto[]>> => {
        const client = new UsersClient(props.messageBroker);
        return client.findAll();
    }


    const [modalTaetigkeiten, setModalTaetigkeiten] = useState({
        title: "",
        value: "",
        index: -1,
        dataset:{},
    });

    const [showTaetigkeiten, setShowTaetigkeiten] = useState(false);

    const toggleModalTaetigkeiten = () => {
        setShowTaetigkeiten(!showTaetigkeiten);
    };

    const [modalKunde, setModalKunde] = useState({
        title: "",
        value: "",
        index: -1,
        dataset:{},
    });

    const [showKunde, setShowKunde] = useState(false);

    const toggleModalKunde = () => {
        setShowKunde(!showKunde);
    };

    const [modalProtocol, setModalProtocol] = useState({
        title: "",
        value: "",
        index: -1,
        dataset:{},
    });


    const [showProtocol, setShowProtocol] = useState(false);

    const [isAdmin, setIsAdmin] = useState(true);

    const toggleModalProtocol = () => {
        setShowKunde(!showProtocol);
    };

    const getFilterValue = (value: number) => {
        for (const element of filterOptions) {
            if (element.value === value) {
                return element;
            }
        }
        return null;
    };

    const filterOptions: {
        label: string,
        value: number,
    }[] = [{
        label: "10",
        value: 10
    }, {
        label: "25",
        value: 25
    }, {
        label: "50",
        value: 50
    },];

    const calcSumWorkReal = (dataset: ResponseTimeTrackingDto): string => {
        const minStart: number = dataset.actualStart?.getHours()??0 * 60 + (dataset.actualStart?.getMinutes()??0);
        const minEnd: number = dataset.actualEnd?.getHours()??0 * 60 + (dataset.actualEnd?.getMinutes()??0);
        const minBreak: number = parseInt((dataset.actualBreakTime??"00:00").split(":")[0]) * 60 + parseInt((dataset.actualBreakTime??"00:00").split(":")[1]);
        return Math.floor((minEnd - minStart - minBreak) / 60 * 100) / 100 + " Stunden";
    }

    const calcSumWorkSoll = (dataset: ResponseTimeTrackingDto): string => {
        const minStart: number = dataset.scheduledStart?.getHours()??0 * 60 + (dataset.scheduledStart?.getMinutes()??0);
        const minEnd: number = dataset.scheduledEnd?.getHours()??0 * 60 + (dataset.scheduledEnd?.getMinutes()??0);
        const minBreak: number = parseInt((dataset.scheduledBreakTime??"00:00").split(":")[0]) * 60 + parseInt((dataset.scheduledBreakTime??"00:00").split(":")[1]);
        return Math.floor((minEnd - minStart - minBreak) / 60 * 100) / 100 + " Stunden";
    }

    const getHoursMins = (test?: Date):string => {
        if(!test) {
            return "00:00";
        }
        const hours:string = (test.getHours()<10?'0'+test.getHours():''+test.getHours());
        const mins:string = (test.getMinutes()<10?'0'+test.getMinutes():''+test.getMinutes());
        return hours+":"+mins;
    }

    const setHoursMins = (test: Date | undefined, dateStr: string) => {
        if(!test) {
            return;
        }
        test.setHours(parseInt(dateStr.split(':')[0]));
        test.setMinutes(parseInt(dateStr.split(':')[1]));
    }

    
    const getDate = (test?: Date):string => {
        if(!test) {
            return "0000-00-00";
        }
        const year:string = ''+test.getFullYear();
        const month:string = ((test.getMonth()+1) > 10 ? ''+(test.getMonth()+1) : '0' + (test.getMonth()+1));
        const day:string = (test.getDate() > 10 ? ''+test.getDate() : '0' + test.getDate());
        return year+"-"+month+"-"+day;
    }

    const setDate = (test: Date | undefined, dateStr: string) => {
        if(!test) {
            return;
        }
        test.setFullYear(parseInt(dateStr.split('-')[0]));
        test.setMonth(parseInt(dateStr.split('-')[1]));
        test.setDate(parseInt(dateStr.split('-')[2]));
    }

    const renderDatasets = () => {
        const output: JSX.Element[] = [];
        let i: number = 0;
        for (const dataset of datasets) {
            const index: number = i;
            output.push(<tr key={i}>
                {isAdmin?<td>
                    <Select
                        menuPortalTarget={document.body}
                        className="select-overview"
                        options={userOptions}
                        defaultValue={userOptions.filter(option => option.value === dataset.email)[0] ?? {}}
                        onChange={(e: any) => {
                            const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                            newDatasets[index].email = e.value;
                            setDatasets(newDatasets);
                        }}
                    />
                </td>:null}
                <td>
                    <span onClick={() => {
                        setModalKunde({
                            title: "Kunde",
                            value: dataset.customerDescription,
                            index,
                            dataset,
                        });
                        setShowKunde(true);
                    }}>{dataset.customerDescription}
                    </span>
                </td>
                <td>
                    <span onClick={() => {
                        setModalTaetigkeiten({
                            title: "Tätigkeit",
                            value: dataset.taskName,
                            index,
                            dataset,
                        });
                        setShowTaetigkeiten(true);
                    }}>{dataset.taskName}</span>
                </td>
                <td>
                    <input className="form-control" type="date" readOnly={isAdmin?false:true} value={getDate(dataset.scheduledStart)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setDate(newDatasets[index].scheduledStart, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" readOnly={isAdmin?false:true} value={getHoursMins(dataset.scheduledStart)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setHoursMins(newDatasets[index].scheduledStart, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" readOnly={isAdmin?false:true} value={getHoursMins(dataset.scheduledEnd)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setHoursMins(newDatasets[index].scheduledEnd, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" readOnly={isAdmin?false:true} value={dataset.scheduledBreakTime} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        newDatasets[index].scheduledBreakTime = e.currentTarget.value;
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>{calcSumWorkSoll(dataset)}</td>
                <td>
                    <input className="form-control" type="date" value={getDate(dataset.actualStart)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setDate(newDatasets[index].actualStart, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" value={getHoursMins(dataset.actualStart)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setHoursMins(newDatasets[index].actualStart, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" value={getHoursMins(dataset.actualEnd)} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        setHoursMins(newDatasets[index].actualEnd, e.currentTarget.value);
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>
                    <input className="form-control" type="time" value={dataset.actualBreakTime} onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        newDatasets[index].actualBreakTime = e.currentTarget.value;
                        setDatasets(newDatasets);
                        updateDataset(dataset);
                    }} />
                </td>
                <td>{calcSumWorkReal(dataset)}</td>
                <td>
                    <span onClick={() => {
                        setModalProtocol({
                            title: "Arbeits Protokoll",
                            value: dataset.workProtocol ?? "",
                            index,
                            dataset,
                        });
                        setShowProtocol(true);
                    }}>{dataset.workProtocol}
                    </span>
                </td>
                {isAdmin?<td>
                    <a href="#" onClick={() => { }}>delete</a>
                </td>:null}
            </tr>);
            i++;
        }
        return output;
    }

    const fetchAll = async () => {
        const client = new TimeTrackingClient(props.messageBroker);
        const start: Date = new Date(Date.now());
        start.setDate(parseInt(filter.start.split("-")[2]));
        start.setMonth(parseInt(filter.start.split("-")[1])-1);
        start.setFullYear(parseInt(filter.start.split("-")[0]));
        start.setHours(10);
        start.setMinutes(0);
        const end: Date = new Date();
        end.setDate(parseInt(filter.end.split("-")[2]));
        end.setMonth(parseInt(filter.end.split("-")[1]) - 1);
        end.setFullYear(parseInt(filter.end.split("-")[0]));
        end.setHours(11);
        end.setMinutes(0);
        console.log(filter.start.split("-"));
        console.log(filter.end.split("-"));
        console.log(start);
        console.log(end);
        const data = (await client.findAll({
            timeRangeStart: start,
            timeRangeEnd: end
        })).data.slice(0, filter.amount - 1);
        setDatasets(data);
    };

    const updateDataset = async (dataset: any) => {
        const client = new TimeTrackingClient(props.messageBroker);
        client.update(dataset);
    };

    


    React.useEffect(() => {
        fetchUserOption().then(async (data) => {
            console.log(data.data);
            const arr: any[] = [];
            for (const user of data.data) {
                arr.push({
                    label: user.email,
                    value: user.email,
                });
            }
            setUserOptions(arr);
            await fetchAll();
        }).catch((error: AxiosError) => {

        });
    }, [])

    return (
        <div>
            <Menu {...props}/>
            <Modal isOpen={showTaetigkeiten} toggle={() => { toggleModalTaetigkeiten(); }}>
                <ModalHeader toggle={() => { toggleModalTaetigkeiten(); }}>{modalTaetigkeiten.title}</ModalHeader>
                <ModalBody><textarea className="form-control" value={modalTaetigkeiten.value} onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                    setModalTaetigkeiten(JSON.parse(JSON.stringify(Object.assign(modalTaetigkeiten, { value: e.currentTarget.value }))));
                }}></textarea></ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        newDatasets[modalTaetigkeiten.index].taskName = modalTaetigkeiten.value;
                        updateDataset(newDatasets[modalTaetigkeiten.index]);
                        setDatasets(newDatasets);
                        setShowTaetigkeiten(false);
                    }}>save</Button>{' '}
                    <Button color="secondary" onClick={() => { setShowTaetigkeiten(false); }}>cancel</Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={showKunde} toggle={() => { toggleModalKunde(); }}>
                <ModalHeader toggle={() => { toggleModalKunde(); }}>{modalKunde.title}</ModalHeader>
                <ModalBody><textarea className="form-control" value={modalKunde.value} onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                    setModalTaetigkeiten(JSON.parse(JSON.stringify(Object.assign(modalKunde, { value: e.currentTarget.value }))));
                }}></textarea></ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        newDatasets[modalKunde.index].customerDescription = modalKunde.value;
                        updateDataset(newDatasets[modalKunde.index]);
                        setDatasets(newDatasets);
                        setShowKunde(false);
                    }}>save</Button>{' '}
                    <Button color="secondary" onClick={() => { setShowKunde(false); }}>cancel</Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={showProtocol} toggle={() => { toggleModalProtocol(); }}>
                <ModalHeader toggle={() => { toggleModalProtocol(); }}>{modalProtocol.title}</ModalHeader>
                <ModalBody><textarea className="form-control" value={modalProtocol.value} onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                    setModalProtocol(JSON.parse(JSON.stringify(Object.assign(modalProtocol, { value: e.currentTarget.value }))));
                }}></textarea></ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => {
                        const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                        newDatasets[modalProtocol.index].workProtocol = modalProtocol.value;
                        updateDataset(newDatasets[modalProtocol.index]);
                        setDatasets(newDatasets);
                        setShowProtocol(false);
                    }}>save</Button>{' '}
                    <Button color="secondary" onClick={() => { setShowProtocol(false); }}>cancel</Button>
                </ModalFooter>
            </Modal>
            <div className="container">
                <div>
                    <h1>Übersicht Aufgaben</h1>
                </div>
                <div className="generic-container">
                    <div style={{ float: "right", textAlign: "center" }}>
                        <div className="row">
                            <div className="col-md-4">
                                Von
                                 <input className="form-control" type="date" value={filter.start} onChange={(e:React.ChangeEvent<HTMLInputElement>)=> {
                                    const newFilter:any = filter;
                                    filter.start = e.currentTarget.value;
                                    setFilter(JSON.parse(JSON.stringify(newFilter)));
                                    fetchAll();
                                }} />
                            </div>
                            <div className="col-md-4">
                                Bis
                                <input className="form-control" type="date" value={filter.end} onChange={(e:React.ChangeEvent<HTMLInputElement>)=> {
                                    const newFilter:any = filter;
                                    filter.end = e.currentTarget.value;
                                    setFilter(JSON.parse(JSON.stringify(newFilter)));
                                    fetchAll();
                                }} />
                            </div>
                            <div className="col-md-4">
                                Anzahl Aufgaben
                                                    <Select
                                    menuPortalTarget={document.body}
                                    className="select-overview"
                                    value={getFilterValue(filter.amount)}
                                    options={filterOptions}
                                    onChange={(e: any) => {
                                        const newFilter: any = filter;
                                        newFilter.amount = e.value;
                                        setFilter(JSON.parse(JSON.stringify(newFilter)));
                                        fetchAll();
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <table className="table table-striped table-dark table-responsive">
                            <thead>
                                <tr>
                                    {isAdmin?<th scope="col">Mitarbeiter</th>:null}
                                    <th scope="col">Kunde</th>
                                    <th scope="col">Tatigkeit</th>
                                    <th scope="col">Soll Arbeitsbegin</th>
                                    <th scope="col">Soll Anfang</th>
                                    <th scope="col">Soll Ende</th>
                                    <th scope="col">Soll Pause</th>
                                    <th scope="col">Soll Arbeitszeit</th>
                                    <th scope="col">Tatsächlicher Arbeitsbegin</th>
                                    <th scope="col">Tatsächlicher Anfang</th>
                                    <th scope="col">Tatsächliches Ende</th>
                                    <th scope="col">Tatsächliche Pause</th>
                                    <th scope="col">Geleistete Arbeitszeit</th>
                                    <th scope="col">Arbeitsprotokoll</th>
                                    {isAdmin?<th scope="col">&nbsp;</th>:null}
                                </tr>
                            </thead>
                            <tbody>
                                {renderDatasets()}
                            </tbody>
                        </table>
                    </div>
                    <div>
                        {isAdmin?<button type="button" className="btn btn-primary" onClick={() => {
                            const start: Date = new Date();
                            start.setHours(10);
                            start.setMinutes(0);
                            const end: Date = new Date();
                            end.setHours(11);
                            end.setMinutes(0);

                            const newTask: CreateTimeTrackingDto = {
                                email: "test@test.de",
                                customerName: "none",
                                customerDescription: "none",
                                taskName: "none",
                                taskDescription: "none",
                                scheduledStart: start,
                                scheduledEnd: end,
                                scheduledBreakTime: "00:30",
                                actualStart: start,
                                actualEnd: end,
                                actualBreakTime: "00:30",
                                workProtocol: "none"
                            };
                            const client = new TimeTrackingClient(props.messageBroker);
                            client.create(newTask).then((response) => {
                                fetchAll();
                            }).catch((err)=>{
                            });
                            /*const newTaskResponse: ResponseTimeTrackingDto = Mocks.createTask(newTask);
                            const newDatasets: ResponseTimeTrackingDto[] = [...datasets];
                            newDatasets.push(newTaskResponse);
                            setDatasets(newDatasets);*/
                           
                        }}>Neue Aufgabe anlegen</button>:null}
                    </div>
                </div>
            </div>
        </div>
    );
}