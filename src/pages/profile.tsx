import React, {ChangeEvent, useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import {UsersClient} from "../message/client/users.client";
import {loginPagePath} from "../page-registry";
import './styles/profil.css';
import {UpdateUserIncludingPasswordDto} from "../common/shared_interfaces/dto/user/update-user-including-password.dto";

interface UserData {
    email: string,
    username: string,
    firstname: string,
    lastname: string,
    password: string,
}

export function Profile(props: PageProps) {
    const currentToken = props.jwtManager.getDecodedToken();
    
    const [newPassword, setNewPassword] = useState("");

    const [userData, setUserData] = useState<Partial<UpdateUserIncludingPasswordDto>>({
        email: 'Testemail@test.de',
        username: 'max.man',
        firstname: 'Max',
        lastname: 'Mustermann',
        password: '',
        newPassword: "",
    });

    const fetchUser = async () => {
        const client = new UsersClient(props.messageBroker);
        client.findOne(currentToken?.sub??-1).then((data)=> {
            console.log(data);
            setUserData(data.data as any);
        }).catch((err)=> {
            console.log(err);
        });
    };


    React.useEffect(() => {
        fetchUser();
    }, [])

    return (
        <div>
            <Menu {...props}/>
            <div>
                <div className="generic-container container">
                    <div>
                        <h1>Profil</h1>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h4>email <small>*</small></h4>
                            <input id="email" type="email" className="form-control" value={userData.email} onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.email = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>username</h4>
                            <input id="username" type="text" className="form-control" defaultValue={userData.username} onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.username = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Vorname</h4>
                            <input id="vorname" type="text" className="form-control" value={userData.firstname} onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.firstname = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Nachname</h4>
                            <input id="nachname" type="text" className="form-control" value={userData.lastname} onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.lastname = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Altes Passwort</h4>
                            <input id="pw" type="password" className="form-control" value={userData.password} onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.password = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <h4>Neues Passwort</h4>
                            <input id="pwNew" type="password" className="form-control" value={userData.password}  onChange={(e:ChangeEvent<HTMLInputElement>)=>{
                                userData.newPassword = e.currentTarget.value;
                                setUserData(JSON.parse(JSON.stringify(userData)));
                            }}/>
                        </div>
                        <div className="col-12">
                            <button type="button" className="btn btn-primary" onClick={()=> {
                                const client = new UsersClient(props.messageBroker);
                                if(currentToken?.sub) {
                                    const update: UpdateUserIncludingPasswordDto = {
                                        id: currentToken?.sub,
                                        //@ts-ignore
                                        password: document.getElementById<HTMLInputElement>('pw')?.value ?? userData.password,
                                        //@ts-ignore
                                        username: document.getElementById<HTMLInputElement>('username')?.value ?? userData.username,
                                        //@ts-ignore
                                        email: document.getElementById<HTMLInputElement>('email')?.value ?? userData.email,
                                        //@ts-ignore
                                        firstname: document.getElementById<HTMLInputElement>('vorname')?.value ?? userData.firstname,
                                        //@ts-ignore
                                        lastname: document.getElementById<HTMLInputElement>('nachname')?.value ?? userData.lastname,
                                        //@ts-ignore
                                        newPassword: document.getElementById<HTMLInputElement>('pwNew')?.value ?? userData.password,
                                    };
                                    client.update(update).then((data)=> {
                                        console.log(data);
                                        props.changePage(loginPagePath);
                                    }).catch((err)=> {
                                        console.log(err);
                                    });
                                }
                            }}>Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}