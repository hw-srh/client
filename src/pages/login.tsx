import * as React from "react";
import {PageProps} from "../interfaces/page-props";
import {dashboard} from "../page-registry";
import './styles/login.css';
import {AuthClient} from "../message/client/auth.client";

export function Login(props: PageProps) {

    const usernameHtmlId = "username_id";
    const passwordHtmlId = "password_id";

    const handleLogin = async () => {

        const password = (document.getElementById(passwordHtmlId) as HTMLInputElement).value;
        const userNameOrEmail = (document.getElementById(usernameHtmlId) as HTMLInputElement).value;

        const authClient = new AuthClient(props);

        try {
            await authClient.login({
                userNameOrEmail,
                password
            })
            props.changePage(dashboard)
        } catch (e) {
            alert("Invalid login credentials!");
        }
    };

    return (
        <div>
            <div className="container">
                <div className="row justify-content-center">
                    <div id="Login-container">
                        <h1>Login</h1>
                        <div className="form-group">
                            <label>Email address / username</label>
                            <input type="text" className="form-control" id={usernameHtmlId} placeholder="insert your username/email" name="username" minLength={8}/>
                            <small id="emailHelp" className="form-text text-muted">Some text for username</small>
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control" placeholder="insert your password" id={passwordHtmlId}  name="pwd" minLength={8}/>
                            <small id="emailHelp" className="form-text text-muted">Some text for password</small>
                        </div>
                        <button type="button" className="btn btn-primary" onClick={handleLogin}>Login</button>
                    </div>
                </div>
            </div>
        </div>
    );
}