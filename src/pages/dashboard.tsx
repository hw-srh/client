import React, {useState} from "react";
import {Menu} from "../components/menu";
import {PageProps} from "../interfaces/page-props";
import './styles/dashboard.css';

import {Bar, Pie} from 'react-chartjs-2';
import { TimeTrackingClient } from "../message/client/time-tracking.client";
import { UsersClient } from "../message/client/users.client";
import { ResponseUserDto } from "../common/shared_interfaces/dto/user/response-user.dto";
import { ResponseTimeTrackingDto } from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";


export function Dashboard(props: PageProps) {
    const [barDataMonth, setBarDataMonth] = useState({
        labels: ["rofl"],
        datasets: [{
            data:[1313]
        }]
    });
    
    const [barDataWoche, setBarDataWoche] = useState({
        labels: ["rofl"],
        datasets: [{
            data:[1313]
        }]
    });

    const [tasks, setTasks] = useState<{
        label:string,
        email:string,
        date:string,
    }[]>([]);

    const fetchAllTasks = async () => {
        const client = new TimeTrackingClient(props.messageBroker);
        const start: Date = new Date(Date.now());
        start.setUTCDate(1);
        const end: Date = new Date(Date.now());
        const data = (await client.findAll({
            timeRangeStart: start,
            timeRangeEnd: end
        }));
        return data.data;
        //setDatasets(data);
    };

    const fetchAllUsers = async () => {
        const client = new UsersClient(props.messageBroker);
        return (await client.findAll()).data;
    };

    React.useEffect(() => {
        (async () => {
            const users = await fetchAllUsers();
            const tasks = await fetchAllTasks();
            calcUnterstundenMonat(users, tasks);
            calcUnterstundenWoche(users, tasks);
            setOpenTasksMonat(users, tasks);
        })();
    }, []);

    const setOpenTasksMonat = (users: ResponseUserDto[], tasks: ResponseTimeTrackingDto[]) => {
        const userMaxHoursInMonth: {[key:string]:number} = {};
        for(const user of users) {
            userMaxHoursInMonth[user.email] = user.workHoursPerWeek;
        }

        const today: Date = new Date(Date.now());
        const firstDayOfMonth:Date = new Date(today.getFullYear(), today.getMonth(), 0 + 1);

        const tasksMap: any[] = [];
        for(const task of tasks) {
            if(!task.actualStart || task.actualStart<firstDayOfMonth) {
                continue;
            }
            tasksMap.push({
                label:task.taskName,
                email: task.email,
                date:task.actualStart.getFullYear()+"-"+(task.actualStart.getMonth()+1)+"-"+task.actualStart.getDate(),
            });
        }
        setTasks(tasksMap as any);
    }

    const calcUnterstundenMonat = (users: ResponseUserDto[], tasks: ResponseTimeTrackingDto[]) => {
        const userMaxHoursInMonth: {[key:string]:number} = {};
        for(const user of users) {
            userMaxHoursInMonth[user.email] = user.workHoursPerWeek;
        }

        const userWorkedTimeMap: {[key:string]:number} = {};
        for(const task of tasks) {
            //@ts-ignore
            let workedTimeInSeconds: number = (task.actualEnd - task.actualStart) / 1000;
            workedTimeInSeconds -= parseInt(task.actualBreakTime?.split(":")[0]??"0") * 60 * 60;
            workedTimeInSeconds -= parseInt(task.actualBreakTime?.split(":")[1]??"0") * 60;
            if(userWorkedTimeMap[task.email] === undefined) {
                userWorkedTimeMap[task.email] = 0;
            }
            userWorkedTimeMap[task.email] += workedTimeInSeconds;
        }

        const diffHours: {[key:string]:number} = {};
        for(const email of Object.keys(userWorkedTimeMap)) {
            const actualWorkHoursInWeek:number = userWorkedTimeMap[email];
            const today: Date = new Date(Date.now());
            const daysInCurrentMonth:number = new Date(today.getFullYear(), today.getMonth()+1, 0).getDate();
            const currentDay: number = today.getDate();
            const percentDaysOfFullMonth: number = (currentDay/daysInCurrentMonth);
            const weeksInMonth: number = daysInCurrentMonth/7;
            const actualTotalWorkHours = actualWorkHoursInWeek/3600 * weeksInMonth * percentDaysOfFullMonth;
            const plannedTotalWorkHours = userMaxHoursInMonth[email] * weeksInMonth * percentDaysOfFullMonth;
            const diff:number = plannedTotalWorkHours-actualTotalWorkHours;
            diffHours[email] = Math.floor(diff*10)/10;
        }
        setDataMonth(diffHours);
    }

    const setDataMonth = (diffHours: {[key:string]:number}) => {
        const datasets:any ={
            datasets: [{
                data: Object.values(diffHours),
                backgroundColor: [
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(0, 163, 51)',
                    'rgb(54, 162, 235)',
                    'rgb(153, 102, 255)',
                    'rgb(201, 203, 207)',
                    'rgb(0,0,255)'
                ],
            }],
            labels: Object.keys(diffHours),
        };

        setBarDataMonth(datasets);
    }

    const calcUnterstundenWoche = (users: ResponseUserDto[], tasks: ResponseTimeTrackingDto[]) => {
        const d:Date = new Date(Date.now());
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        const firstDayOfWeek: Date = new Date(d.setDate(diff));

        const userMaxHoursInMonth: {[key:string]:number} = {};
        for(const user of users) {
            userMaxHoursInMonth[user.email] = user.workHoursPerWeek;
        }

        const userWorkedTimeMap: {[key:string]:number} = {};
        for(const task of tasks) {
            if(!task.actualStart || task.actualStart<firstDayOfWeek)  {
                continue;
            }
            //@ts-ignore
            let workedTimeInSeconds: number = (task.actualEnd - task.actualStart) / 1000;
            workedTimeInSeconds -= parseInt(task.actualBreakTime?.split(":")[0]??"0") * 60 * 60;
            workedTimeInSeconds -= parseInt(task.actualBreakTime?.split(":")[1]??"0") * 60;
            if(userWorkedTimeMap[task.email] === undefined) {
                userWorkedTimeMap[task.email] = 0;
            }
            userWorkedTimeMap[task.email] += workedTimeInSeconds;
        }

        const diffHours: {[key:string]:number} = {};
        for(const email of Object.keys(userWorkedTimeMap)) {
            const actualWorkHoursInWeek:number = userWorkedTimeMap[email];
            const today: Date = new Date(Date.now());
            const daysInCurrentMonth:number = new Date(today.getFullYear(), today.getMonth()+1, 0).getDate();
            const currentDay: number = today.getDate();
            const percentDaysOfFullMonth: number = (currentDay/daysInCurrentMonth);
            const weeksInMonth: number = daysInCurrentMonth/7;
            const actualTotalWorkHours = actualWorkHoursInWeek/3600 * weeksInMonth * percentDaysOfFullMonth;
            const plannedTotalWorkHours = userMaxHoursInMonth[email] * weeksInMonth * percentDaysOfFullMonth;
            const diff:number = plannedTotalWorkHours-actualTotalWorkHours;
            diffHours[email] = Math.floor(diff*10)/10;
        }
        setDataWoche(diffHours);
    }

    const setDataWoche = (diffHours: {[key:string]:number}) => {
        const datasets:any ={
            datasets: [{
                data: Object.values(diffHours),
                backgroundColor: [
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(0, 163, 51)',
                    'rgb(54, 162, 235)',
                    'rgb(153, 102, 255)',
                    'rgb(201, 203, 207)',
                    'rgb(0,0,255)'
                ],
            }],
            labels: Object.keys(diffHours),
        };

        setBarDataWoche(datasets);
    }
    
    const renderTasks = () => {
        const tasksItems:JSX.Element[] = [];
        let i:number = 0;
        for(const task of tasks) {
            tasksItems.push(<div className="dashboard-task-item" key={i}>
                <span><b>{task.label}</b></span>
                <span style={{
                    marginLeft:"5px",
                    fontSize:"60%"
                }}>{task.email}</span>
                <small>{parseDate(task.date)}</small>
            </div>);
            i++;
        }
        return tasksItems;
    };

    const parseDate = (date:string) => {
        const d_real_anfang:Date = new Date();
        d_real_anfang.setFullYear(parseInt(date.split("-")[0]));
        d_real_anfang.setMonth(parseInt(date.split("-")[1]));
        d_real_anfang.setDate(parseInt(date.split("-")[2]));
        return d_real_anfang.toLocaleDateString();
    };

    return <div>
        <Menu {...props}/>
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-6">
                        <h1>Unterstunden Monat</h1>
                        <div className="dashboard-border">
                            <Pie 
                                data={barDataMonth}
                                options={{ maintainAspectRatio: true }}
                            />
                        </div>
                    </div>
                    <div className="col-6">
                        <h1>Unterstunden Woche</h1>
                        <div className="dashboard-border">
                            <Pie 
                                data={barDataWoche}
                                options={{ maintainAspectRatio: true }}
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <h1>Offene Aufgaben diesen Monat</h1>
                        <div className="dashboard-border">
                            <div className="dashboard-tasks-container">
                                {renderTasks()}
                            </div>
                        </div>
                    </div>
                    <div className="col-6"/>
                </div>
            </div>
        </div>
    </div>;
}