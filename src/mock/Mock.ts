import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {CreateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {UpdateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/update-time-tracking.dto";

export interface EmailOption {
    label: string,
    value: string,
};

export class Mocks {
    public static isAdmin: true;

    public static testEmails: string[] = ["test@test.de", "test2@test.de"];

    public static testTask: ResponseTimeTrackingDto = {
        id: "1",
        customerName: "test",
        scheduledBreakTime: "00:30",
        actualBreakTime: "00:30",
        email: "test@test.de",
        customerDescription: "test",
        taskName: "test",
        taskDescription: "test",
        scheduledStart: (new Date("2021-01-11T10:00:00.000Z")),
        scheduledEnd: (new Date("2021-01-11T11:00:00.000Z")),
        actualStart: (new Date("2021-01-11T10:00:00.000Z")),
        actualEnd: (new Date("2021-01-11T11:00:00.000Z")),
        workProtocol: "test",
    };

    public static createTask(task: CreateTimeTrackingDto): ResponseTimeTrackingDto {
        return Object.assign(task, {
            id: "2",
            scheduledStartDate: (new Date("2021-01-11T00:00:00.000Z")),
            scheduledBreakTime: "00:30",
            actualDate: (new Date("2021-01-11T00:00:00.000Z")),
            actualBreakTime: "00:30",
        });
    }

    public static updateTask(task: UpdateTimeTrackingDto): ResponseTimeTrackingDto {
        return Object.assign(task, this.testTask);
    }
}