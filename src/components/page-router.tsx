import * as React from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {homePagePath, loginPagePath, notLoggedInPageRouteConfigs, pageRouteConfigs} from "../page-registry";
import {MessageBroker} from "../message/message-broker";
import {JwtManager} from "../auth/jwt-manager";
import {PageProps} from "../interfaces/page-props";

export const reloadApp = () => {
    window.location.pathname = '/';
};

class PageRouter extends React.Component<any, any> {
    private readonly messageBroker: MessageBroker;
    private readonly jwtManager: JwtManager;
    private readonly pageProps: PageProps;

    public constructor(props: any) {
        super(props);
        this.jwtManager = new JwtManager();
        this.messageBroker = new MessageBroker(
            this.jwtManager,
            (path: string) => this.changePage(path)
        );

        this.pageProps = {
            messageBroker: this.messageBroker,
            changePage: (uriPath: string) => this.changePage(uriPath),
            refreshPage: () => this.refreshPage(),
            reloadApp: () => reloadApp(),
            jwtManager: this.jwtManager,
        };
    }

    render() {
        return (
            <Switch>
                {this.renderPageRoutes()}
            </Switch>
        );
    }

    private renderPageRoutes(): JSX.Element[] {

        const routes: JSX.Element[] = [
            // workaround to force a page reload (rerender) without sending a request to the server
            <Route exact={true} path="/reload" key="reload" component={undefined}/>
        ];

        for (const [pagePath, Component] of this.getRouteConfigs()) {
            routes.push(
                <Route
                    exact={true}
                    path={pagePath}
                    key={pagePath}
                    render={() => <Component {...this.pageProps} />}
                />
            );
        }

        // this route has to be added last otherwise it will match all routes
        routes.push(
            <Route key={'page_does_not_exist'}>
                <Redirect to={{
                    pathname: this.getDefaultPath()
                }}/>
            </Route>
        );

        return routes;
    }

    private getDefaultPath() {

        if (!this.jwtManager.hasToken()) {
            return loginPagePath;
        }

        const lastPathInHistory = this.props.history.location.pathname;

        if (lastPathInHistory in pageRouteConfigs) {
            return lastPathInHistory;
        }

        console.log('Warning a page with path: ' + lastPathInHistory + ' does not exist!');

        return homePagePath;
    }

    private changePage(path: string) {
        this.props.history.push(path);
    }

    private refreshPage() {
        const currentPagePath = this.props.history.location.pathname;
        // workaround to force a page reload (rerender) without sending a request to the server
        this.props.history.replace('/reload');
        this.props.hisotry.replace(currentPagePath);
    }

    private getRouteConfigs(): [string, <T extends PageProps>(props: T) => JSX.Element][] {
        let pageRouteConfigsToUse = pageRouteConfigs;

        // only render the login and password lost page if the user is not authorized
        if (!this.jwtManager.hasToken()) {
            pageRouteConfigsToUse = notLoggedInPageRouteConfigs;
        }

        return Object.entries(pageRouteConfigsToUse);
    }
}

export default withRouter(PageRouter);