import * as React from "react";
import { PageProps } from "../interfaces/page-props";
import { pageRouteConfigs } from "../page-registry";
import './styles/menu.css';

export function Menu(props: PageProps) {
    const [state, setState] = React.useState({
        isOpen: false,
    });

    const closeMenu = () => {
        setState({
            isOpen: false,
        });
    }

    const openMenu = () => {
        setState({
            isOpen: true,
        });
    }

    /*const renderItems = () => {
        const items:JSX.Element[] = [];
        let i:number=0;
        for(const item of Object.keys(pageRouteConfigs)) {
            items.push(<a key={i} href={"/app"+item} onClick={(e:React.MouseEvent<HTMLAnchorElement>)=>{
                e.preventDefault();
                e.stopPropagation();
                props.changePage(item);
            }}>{item}</a>);
            i++;
        }
        return items;
    }*/

    const renderItems = () => {
        const mapping:{[key:string]:string} = {
            "Dashboard":"/dashboard",
            "Aufgaben":"/overview",
            "Benutzerverwaltung":"/overview_user",
            "Excel upload":"/user_upload",
            "Profil":"/profile",
            "Logout":"/login",
        };

        const items:JSX.Element[] = [];
        let i:number=0;
        for(const item of Object.keys(mapping)) {
            items.push(<a key={i} href={"/app"+mapping[item]} onClick={(e:React.MouseEvent<HTMLAnchorElement>)=>{
                e.preventDefault();
                e.stopPropagation();
                props.changePage(mapping[item]);
            }}>{item}</a>);
            i++;
        }
        return items;
    }

    return (
        <>
            <div id="sidemenu-bar">
                <div>
                    <i className="fas fa-bars" onClick={openMenu}/>
                </div>
            </div>
            <div id="sidemenu" style={{width:(state.isOpen?'300px':'0')}} className="sidenav">
                <a href="#" className="closebtn" onClick={closeMenu}>&times;</a>
                {renderItems()}
            </div>
        </>
    );
}