import {MessageBroker} from "../message/message-broker";
import {JwtManager} from "../auth/jwt-manager";

export interface PageProps {
    messageBroker: MessageBroker;
    jwtManager: JwtManager;
    changePage: (path: string) => void;
    refreshPage: () => void;
    reloadApp: () => void;
}