import jwt_decode from "jwt-decode";
import {JwtPayload} from "../common/shared_interfaces/jwt-payload.interface";

export class JwtManager {

    private readonly JWT_ID_IN_LOCAL_STORAGE = 'jwt';
    private decodedToken: JwtPayload | null;

    constructor(private readonly localStorage: Storage = window.localStorage) {
        this.decodedToken = this.decodeToken();
    }

    getToken(): string {
        return this.localStorage.getItem(this.JWT_ID_IN_LOCAL_STORAGE) ?? '';
    }

    getDecodedToken(): JwtPayload | null {
        return this.decodedToken ?? this.decodeToken();
    }

    async setNewToken(jwt: string) {
        this.localStorage.setItem(this.JWT_ID_IN_LOCAL_STORAGE, jwt);
        this.decodeToken();
    }

    hasToken(): boolean {
        return this.getToken().length > 0;
    }

    invalidateToken() {
        this.localStorage.removeItem(this.JWT_ID_IN_LOCAL_STORAGE);
        this.decodedToken = null;
    }

    private decodeToken(): JwtPayload | null {
        try {
            return jwt_decode(this.getToken());
        } catch (e) {
            return null;
        }
    }

    hasTokenExpired() {
        const decodedToken = this.getDecodedToken();
        return Math.floor((Date.now() / 1000)) > (decodedToken?.exp ?? 0);
    }
}