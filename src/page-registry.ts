import {Home} from "./pages/home";
import {Login} from "./pages/login";
import {Overview} from "./pages/overview";
import {Dashboard} from "./pages/dashboard";
import {PasswordReset} from "./pages/passwordReset";
import {PageProps} from "./interfaces/page-props";
import {PasswordLost} from "./pages/passwordLost";
import {UserUpload} from "./pages/userUpload";
import {OverviewUser} from "./pages/overviewUser";
import {Profile} from "./pages/profile";

export const homePagePath = '/home';
export const loginPagePath = '/login';
export const overviewPage = '/overview';
export const overviewPageUser = '/overview_user';
export const dashboard = '/dashboard';
export const passwordReset = '/password-reset/*';
export const passwordLost = '/password_lost';
export const userUpload = '/user_upload';
export const profile = '/profile';

export type PageRouteConfig = { [key: string]: <T extends PageProps>(props: T) => JSX.Element };

export const pageRouteConfigs: PageRouteConfig = {
    [homePagePath]: Home,
    [loginPagePath]: Login,
    [overviewPage]: Overview,
    [overviewPageUser]: OverviewUser,
    [dashboard]: Dashboard,
    [passwordReset]: PasswordReset,
    [passwordLost]: PasswordLost,
    [userUpload]: UserUpload,
    [profile]: Profile
};

export const notLoggedInPageRouteConfigs: PageRouteConfig = {
    [loginPagePath]: Login,
    [passwordLost]: PasswordLost,
    [passwordReset]: PasswordReset
};